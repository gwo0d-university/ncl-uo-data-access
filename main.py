try:
    from functions import *
except ImportError:
    print("Necessary libraries unavailable.")

sensors = ["PER_EMOTE_2602_AIR", "PER_EMOTE_2408_AIR", "PER_EMOTE_2406_AIR", "PER_EMOTE_2405_AIR", "PER_EMOTE_2407_AIR", "PER_EMOTE_2402_AIR", "PER_EMOTE_2401_AIR", "PER_EMOTE_2202_AIR"]

print(get_x_Records(sensors, 1))