try:
    import requests
    import json
    import multiprocessing
except ImportError:
    print("Necessary libraries unavailable.")


def get_x_Records(sensors, x):
    try:
        API_Base_URL = "http://uoweb3.ncl.ac.uk/api/v1.1/sensors/"
        API_CSV_Postfix = "/data/json/"

        records_To_Retrieve = x
        params = "?last_n_records=" + str(records_To_Retrieve)

        RAW_Requests = []

        for i in range(len(sensors)):
            request_URL = API_Base_URL + sensors[i] + API_CSV_Postfix + params

            r = requests.get(request_URL)
            RAW_Requests.append(r.json())

        parsed_Data = []

        for j in range(len(RAW_Requests)):
            parsed_JSON = RAW_Requests[j]
            parsed_Data.append(parsed_JSON["sensors"][0]["data"])

        return parsed_Data
    except Exception:
        return "Error! Please try again..."